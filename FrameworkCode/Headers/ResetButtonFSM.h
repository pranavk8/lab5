/****************************************************************************

  Header file for ResetButton Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMResetButton_H
#define FSMResetButton_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  Debouncing, Ready2Sample
} ResetButtonState_t;

// Public Function Prototypes

bool InitResetButtonFSM(uint8_t Priority);
bool PostResetButtonFSM(ES_Event_t ThisEvent);
bool CheckResetButtonEvents(void);
ES_Event_t RunResetButtonFSM(ES_Event_t ThisEvent);
ResetButtonState_t QueryResetButtonSM(void);

#endif /* FSMResetButton_H */

