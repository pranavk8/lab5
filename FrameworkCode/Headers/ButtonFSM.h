/****************************************************************************

  Header file for Button Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMButton_H
#define FSMButton_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  Debouncing, Ready2Sample
}ButtonState_t;

// Public Function Prototypes

bool InitButtonFSM(uint8_t Priority);
bool PostButtonFSM(ES_Event_t ThisEvent);
bool CheckButtonEvents(void);
ES_Event_t RunButtonFSM(ES_Event_t ThisEvent);
ButtonState_t QueryButtonSM(void);

#endif /* FSMButton_H */

