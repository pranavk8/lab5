/****************************************************************************

  Header file for Step service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServStep_H
#define ServStep_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitStepService(uint8_t Priority);
bool PostStepService(ES_Event_t ThisEvent);
ES_Event_t RunStepService(ES_Event_t ThisEvent);

#endif /* ServStep_H */

