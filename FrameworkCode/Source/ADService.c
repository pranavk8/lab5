/****************************************************************************
 Module
   ADService.c

 Revision
   1.0.1

 Description
   This is a AD file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from ADFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// the headers to access the TivaWare Library
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"
#include "ES_Events.h"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ADService.h"
#include "ADMulti.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint16_t ScaledStepTime;
static uint32_t ADResults[4]; //Analog input variable
static uint16_t AnalogVal, MinTime, MaxTime; 
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitADService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitADService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  ADC_MultiInit(1);   //Initialize one A-D pin PE0
  ADC_MultiRead(ADResults);
  AnalogVal = ADResults[0];   //Reading the pot
	printf("Step time from pot is %u \r\n", AnalogVal);
	MinTime = 2;
	MaxTime = 1000;
  ScaledStepTime = MinTime + (AnalogVal/4095)*(MaxTime - MinTime);
  ES_Timer_InitTimer(ADTimer, 20);    //Initialize AD timer
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostADService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostADService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunADService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunADService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == ADTimer){
    ADC_MultiRead(ADResults);
    AnalogVal = ADResults[0];
		float slope = (AnalogVal/4095.0f);
		//printf("%.4f", slope);
    ScaledStepTime = (uint16_t)MinTime + slope*(MaxTime - MinTime);
		//printf("Step time from pot is %u \r\n", ScaledStepTime);
    ES_Timer_InitTimer(ADTimer, 20);
  }
  return ReturnEvent;
}

uint16_t QueryScaledStepTime(void){
  return ScaledStepTime;
}
/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

