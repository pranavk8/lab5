/****************************************************************************
 Module
   StepService.c

 Revision
   1.0.1

 Description
   This is a Step file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from StepFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"
#include "ES_Events.h"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "StepService.h"
#include "ADService.h"
#include "PWM16Tiva.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void WritePWM(uint8_t row[]);
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint8_t PWMArray[16][4] =  {
  {71, 0, 71, 0},
  {92, 0, 38, 0},
  {100, 0, 0, 0},
  {92, 0, 0, 38},
  {71, 0, 0, 71},
  {38, 0, 0, 92},
  {0, 0, 0, 100},
  {0, 38, 0, 92},
  {0, 71, 0, 71},
  {0, 92, 0, 38},
  {0, 100, 0, 0},
  {0, 92, 38, 0},
  {0, 71, 71, 0},
  {0, 38, 92, 0},
  {0, 0, 100, 0},
  {38, 0, 92, 0},
};
static int StartInd, CurrInd, StepSize;
static uint16_t StepTime;
static bool direction;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitStepService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitStepService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  PWM_TIVA_Init(4);   //Initialize 4 PWM channels
  PWM_TIVA_SetFreq(1000, 0);
  PWM_TIVA_SetFreq(1000, 1);
  
  HWREG(SYSCTL_RCGCGPIO) |= BIT1HI;
  while((HWREG(SYSCTL_PRGPIO) & BIT1HI) != BIT1HI)
  {
  }
  //Enable and set PB0 and PB1 to output
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= (BIT0HI | BIT1HI);
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= (BIT0HI | BIT1HI);
  
  //0 for full step, 2 for wave, 0 for half step, 0 for micro step
  StartInd = 2;
  CurrInd = StartInd;
  //4 for full step, 4 for wave, 2 for half step, 1 for micro step
  StepSize = 4;   
  uint8_t row[4];
	direction = true;
  for(int i=0; i<4; i++){
    row[i] = PWMArray[CurrInd][i];
//		if (i<3){
//			printf("%u, ", row[i]);
//		}
//		else printf("%u \r\n", row[i]);
  }
  WritePWM(row);
  StepTime = QueryScaledStepTime();
  printf("%u \r\n", StepTime);
	
  ES_Timer_InitTimer(StepTimer, StepTime);
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostStepService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostStepService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunStepService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunStepService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  StepTime = QueryScaledStepTime();
  printf("%u \r\n", StepTime);
	
  if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == StepTimer){
    ES_Timer_InitTimer(StepTimer, StepTime);
		if (direction){
			CurrInd = CurrInd + StepSize;
			if (CurrInd>15){
				CurrInd = CurrInd - 16;
			}
		}
		else {
			if ((CurrInd - StepSize) < 0){
				CurrInd = 16 - StepSize + StartInd;
			}
			else{
				CurrInd = CurrInd - StepSize;
			}
		}
    uint8_t row[4];
    for(int i=0; i<4; i++){
      row[i] = PWMArray[CurrInd][i];
//			if (i<3){
//				printf("%u, ", row[i]);
//			}
//			else printf("%u \r\n", row[i]);
    }
    WritePWM(row);
  }
	
	if (ThisEvent.EventType == DBButtonDown){
		printf("Button pressed\r\n");
		direction = !direction;
	}
  
  
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
static void WritePWM(uint8_t row[])
{
  if(row[0] == 0 && row[1] == 0){
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &= BIT0LO; //Write PB0 LOW
  }
  else{
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= BIT0HI;
    PWM_TIVA_SetDuty(row[0], 0);
    PWM_TIVA_SetDuty(row[1], 1);
  }
  
  if(row[2] == 0 && row[3] == 0){
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &= BIT1LO; //Write PB1 LOW
  }
  else{
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= BIT1HI;
    PWM_TIVA_SetDuty(row[2], 2);
    PWM_TIVA_SetDuty(row[3], 3);
  }
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

